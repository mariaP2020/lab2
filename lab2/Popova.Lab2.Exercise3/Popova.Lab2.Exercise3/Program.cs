﻿using System;

namespace Popova.Lab2.Exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            // f(x,y,z) = 1/11 + 3/In(|y|) + z/3 + min(x,x*2)
            Console.WriteLine("введите x:");
            string xstr = Console.ReadLine();
            string ystr = Console.ReadLine();
            string zstr = Console.ReadLine();

            double x = Convert.ToDouble(xstr);
            double y = Convert.ToDouble(ystr);
            double z = Convert.ToDouble(zstr);

            double f;
            f = 1 / 11.0 + Math.Log(Math.Abs(3 / y)) + z / 3 + Math.Max(x, x * 2);
            Console.WriteLine(f);

        }
    }
}
